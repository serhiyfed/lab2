package application;

import vehicles.Bicycle;

/**
 * @author Serhiy Fedurtsya
 * */
public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] bikes = new Bicycle[4];

        bikes[0] = new Bicycle("Trek", 24, 30.0);
        bikes[1] = new Bicycle("Giant", 21, 28.0);
        bikes[2] = new Bicycle("Specialized", 27, 32.0);
        bikes[3] = new Bicycle("Cannondale", 24, 30.0);

        for (Bicycle bike : bikes) {
            System.out.println(bike);
        }
    }
}